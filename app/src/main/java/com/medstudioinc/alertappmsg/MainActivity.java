package com.medstudioinc.alertappmsg;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.alert.xAlert;

public class MainActivity extends AppCompatActivity {
    private Button showAlertMsg;
    private ViewGroup mAltParent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAltParent = (ViewGroup) findViewById(R.id.alt_parent);
        showAlertMsg = (Button) findViewById(R.id.show_alert_msg);

        showAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                xAlert.ToastBarView(MainActivity.this, mAltParent, "Text Message Here!", R.color.alert);
            }
        });
    }
}
