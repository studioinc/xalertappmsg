package com.example.alert;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class xAlert {
    public static void ToastBarView(Activity activity, ViewGroup viewGroup, int message) {
        viewGroup.setVisibility(View.VISIBLE);
        AppMsg appMsg = null != null ? null : AppMsg.makeText(activity, activity.getResources().getString(message), new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red));
        appMsg.setPriority(Integer.MIN_VALUE);
        appMsg.setParent(viewGroup);
        if (true) {
            appMsg.setAnimation(android.R.anim.slide_in_left, android.R.anim.slide_out_right); // in 17432578 : out 17432579
        }
        appMsg.show();
    }

    public static void ToastBarView(Activity activity, ViewGroup viewGroup, int message, int btmMargin) {
        viewGroup.setVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) viewGroup.getLayoutParams();
        params.bottomMargin = btmMargin;
        AppMsg appMsg = null != null ? null : AppMsg.makeText(activity, activity.getResources().getString(message), new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red));
        appMsg.setPriority(Integer.MIN_VALUE);
        appMsg.setLayoutParams(params);
        appMsg.setParent(viewGroup);
        if (true) {
            appMsg.setAnimation(android.R.anim.slide_in_left, android.R.anim.slide_out_right); // in 17432578 : out 17432579
        }
        appMsg.show();
    }

    public static void ToastBarView(Activity activity, ViewGroup viewGroup, String message, int btmMargin, int topMargin) {
        viewGroup.setVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) viewGroup.getLayoutParams();
        params.bottomMargin = btmMargin;
        params.topMargin = topMargin;
        AppMsg appMsg = null != null ? null : AppMsg.makeText(activity, message, new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red));
        appMsg.setPriority(Integer.MIN_VALUE);
        appMsg.setLayoutParams(params);
        appMsg.setParent(viewGroup);
        if (true) {
            appMsg.setAnimation(android.R.anim.slide_in_left, android.R.anim.slide_out_right); // in 17432578 : out 17432579
        }
        appMsg.show();
    }

    public static void ToastBarView(Activity activity, ViewGroup viewGroup, String message, int btmMargin, int topMargin, int color) {
        viewGroup.setVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) viewGroup.getLayoutParams();
        params.bottomMargin = btmMargin;
        params.topMargin = topMargin;
        AppMsg appMsg = null != null ? null : AppMsg.makeText(activity, message, new AppMsg.Style(AppMsg.LENGTH_SHORT, color));
        appMsg.setPriority(Integer.MIN_VALUE);
        appMsg.setLayoutParams(params);
        appMsg.setParent(viewGroup);
        if (true) {
            appMsg.setAnimation(android.R.anim.slide_in_left, android.R.anim.slide_out_right); // in 17432578 : out 17432579
        }
        appMsg.show();
    }

    public static void ToastBarView(Activity activity, ViewGroup viewGroup, String message, int btmMargin, int topMargin, int bgColor, int txtColor) {
        viewGroup.setVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) viewGroup.getLayoutParams();
        params.bottomMargin = btmMargin;
        params.topMargin = topMargin;
        AppMsg appMsg = null != null ? null : AppMsg.makeText(activity, message, new AppMsg.Style(AppMsg.LENGTH_SHORT, bgColor));
        appMsg.setPriority(Integer.MIN_VALUE);
        appMsg.setLayoutParams(params);
        appMsg.setParent(viewGroup);
        appMsg.textView.setTextColor(activity.getResources().getColor(txtColor));
        if (true) {
            appMsg.setAnimation(android.R.anim.slide_in_left, android.R.anim.slide_out_right); // in 17432578 : out 17432579
        }
        appMsg.show();
    }

    public static void ToastBarView(Activity activity, ViewGroup viewGroup, String  message) {
        viewGroup.setVisibility(View.VISIBLE);
        AppMsg appMsg = null != null ? null : AppMsg.makeText(activity, message, new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red));
        appMsg.setPriority(Integer.MIN_VALUE);
        appMsg.setParent(viewGroup);
        if (true) {
            appMsg.setAnimation(android.R.anim.slide_in_left, android.R.anim.slide_out_right); // in 17432578 : out 17432579
        }
        appMsg.show();
    }

    public static void ToastBarView(Activity activity, ViewGroup viewGroup, String  message, int color) {
        viewGroup.setVisibility(View.VISIBLE);
        AppMsg appMsg = null != null ? null : AppMsg.makeText(activity, message, new AppMsg.Style(AppMsg.LENGTH_SHORT, color));
        appMsg.setPriority(Integer.MIN_VALUE);
        appMsg.setParent(viewGroup);
        if (true) {
            appMsg.setAnimation(android.R.anim.slide_in_left, android.R.anim.slide_out_right); // in 17432578 : out 17432579
        }
        appMsg.show();
    }
}
